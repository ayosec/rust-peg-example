# simplecalc

Minimal example of [rust-peg](https://github.com/kevinmehall/rust-peg)

## Usage

```bash
$ cargo build --release
$ target/release/simplecalc '5 * 8 * (1 + 2)'
120
```
