use std::env::args;

mod parser {
    include!(concat!(env!("OUT_DIR"), "/parser.rs"));
}

fn main() {
    match parser::expr(&args().skip(1).next().unwrap()) {
        Ok(r) => println!("{:?}", r),
        Err(e) => println!("Parse error: {}", e),
    }
}
